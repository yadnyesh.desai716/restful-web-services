package com.in28minutes.rest.webservice.restfulwebservices.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
public class SpringSecurityConfiguration {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        //  Step 1. All request must be authenticated.
        http.authorizeHttpRequests(
                auth -> auth.anyRequest().authenticated());

        //  Step 2. If a request is not authenticated, a web page is shown
        http.formLogin(withDefaults());

        // Step 3. Disable CSRF so that POST and PUT requests can be enabled
        http.csrf(csrf -> csrf.disable());

        //  Step 4. Prevent the header from being added to the response.
        http.headers(header -> header.frameOptions(frameOptions -> frameOptions.disable()));

        /*http.csrf().disable();
        http.headers().frameOptions().disable();*/

        return http.build();
    }
}
