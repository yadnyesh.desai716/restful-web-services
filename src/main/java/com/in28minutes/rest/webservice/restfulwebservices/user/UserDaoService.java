package com.in28minutes.rest.webservice.restfulwebservices.user;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class UserDaoService {
    //  User DAO Data Access Object

    private static Integer userCount = 0;

    private static final List<User> users = new ArrayList<>(List.of(
            new User(++userCount, "Adam", LocalDate.now().minusYears(30)),
            new User(++userCount, "Eve", LocalDate.now().minusYears(25)),
            new User(++userCount, "Jim", LocalDate.now().minusYears(20))
    ));

    //  Find all users
    public List<User> findAll() {
        return users;
    }

    //  Save a user
    public User save(User user) {
        user.setId(++userCount);
        users.add(user);
        return user;
    }

    //  Find only one user
    public User findOne(Integer id) {
        return users.stream().filter(user -> Objects.equals(user.getId(), id)).findFirst().orElse(null);
    }

    //  Delete a user.
    public void deleteById(int id) {
        users.removeIf(user -> user.getId().equals(id));
    }
}
