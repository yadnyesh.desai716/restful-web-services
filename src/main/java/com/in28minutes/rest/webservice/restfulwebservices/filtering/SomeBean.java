package com.in28minutes.rest.webservice.restfulwebservices.filtering;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;

//@JsonIgnoreProperties({"field1", "field2"}) //  Defining at the class level
@JsonFilter("SomeBeanFilter")   //  Dynamic filtering
public class SomeBean {

    private final String field1;
    private final String field2;
    private final String field3;
    @JsonIgnore //  Static Filtering: field4 will be ignored for all REST APIs.
    private final String field4;

    public SomeBean(String field1, String field2, String field3, String field4) {
        this.field1 = field1;
        this.field2 = field2;
        this.field3 = field3;
        this.field4 = field4;
    }

    public String getField1() {
        return field1;
    }

    public String getField2() {
        return field2;
    }

    public String getField3() {
        return field3;
    }

    public String getField4() {
        return field4;
    }

    @Override
    public String toString() {
        return "SomeBean{" +
                "field1='" + field1 + '\'' +
                ", field2='" + field2 + '\'' +
                ", field3='" + field3 + '\'' +
                '}';
    }
}
